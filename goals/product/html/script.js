        window.onload = slideContent;

        function slideContent() {
            $('body').animate({'left':'0%'}, 250);
        }
        
        'use strict';
        $( document ).ready(function() {

            $('.close-icon').click(function(){
                $('.layer').removeClass('visible');
            });

            (function($) {
                $('.accordion a').click(function(j) {
                    var dropDown = $(this).closest('li').find('article');

                    $(this).closest('.accordion').find('article').not(dropDown).slideUp();

                    if ($(this).hasClass('active')) {
                        $(this).removeClass('active');
                    } else {
                        $(this).closest('.accordion').find('a.active').removeClass('active');
                        $(this).addClass('active');
                    }

                    dropDown.stop(false, true).slideToggle();

                    j.preventDefault();
                });
            })(jQuery);

        });